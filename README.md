# SnapFit

Pașii de compilare și rulare:

1. Se descarcă codul
2. Se importează proiectul în Android Studio IDE
3. Se creează un Android Virtual Device în Android Studio IDE cu API Level de minim 24 (preferabil >=30)
4. Se apasă pe butonul Run din Android Studio IDE și începe Build-ul
5. Odată ce sa completat Build-ul, Android Virtual Device-ul creat va porni cu aplicația mobilă